" syntax zapnut
syntax on 
set confirm

" lepsi kompletace cmdliny
set wildmenu
"
set hlsearch
set nomodeline
set ignorecase
set smartcase
set backspace=indent,eol,start

" formatovani textu
set autoindent
set nostartofline
set ruler
set laststatus=2
set confirm
set novb
set mouse=a
set cmdheight=2
set showmatch
set nojoinspaces
" podoba status liny
set statusline=%1*%n:%*\ %<%f\ %y%m%2*%r%*%=[%b,0x%B]\ \ %l/%L,%c%V\ \ %P

"cisla 
set number
set notimeout ttimeout ttimeoutlen=200
set pastetoggle=<F11>
set ch=2
set mousehide

set smartindent
set shiftwidth=4
set softtabstop=4
set expandtab
set showcmd
map Y y$
"set foldmethod=syntax

set keywordprg=info
set nocompatible 
filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Plugin 'gmarik/vundle'

Plugin 'tpope/vim-fugitive'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'vim-scripts/taglist.vim'
Plugin 'scrooloose/syntastic'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'tomtom/tlib_vim'
Plugin 'tomtom/tcomment_vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'LaTeX-Box-Team/LaTeX-Box'
Plugin 'tpope/vim-surround'
Plugin 'kien/tabman.vim'
Plugin 'tsaleh/vim-matchit'
Plugin 'vim-scripts/a.vim'
Plugin 'othree/xml.vim'
Plugin 'vim-scripts/DoxygenToolkit.vim'
Plugin 'majutsushi/tagbar'
Plugin 'bling/vim-airline'
Plugin 'tpope/vim-abolish'
Plugin 'tpope/vim-repeat'
Plugin 'Mizuchi/STL-Syntax'
Plugin 'benmills/vimux'
Plugin 'tpope/vim-endwise'
Plugin 'terryma/vim-expand-region'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'sjl/gundo.vim'
Plugin 'oblitum/rainbow'


"Bundle 'rstacruz/sparkup',{'rtp','vim/'}
"Bundle 'tpope/vim-rails.git'


filetype plugin indent on 
"Bundle 'L9'
"Bundle 'FuzzyFinder'
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

"== ost
filetype on
let g:syntastic_mode_map = { "mode": "passive",
            \ "active_filetypes": [],
            \ "passive_filetypes": [] }
let g:ycm_register_as_syntastic_checker=0
let g:ycm_show_diagnostics_ui = 0
let g:loaded_syntastic_cpp_cpplint_checker=1
nmap <F5> :SyntasticCheck<CR>
let g:syntastic_cpp_check_header=1
let g:syntastic_c_check_header = 1
let g:syntastic_enable_sign=1
let g:syntastic_cpp_compiler_options = ' -std=c++0x' 
let g:syntastic_error_symbol='✗'
let g:syntastic_warning_symbol='⚠'
let g:syntastic_enable_balloone = 1
let g:syntastic_cpp_include_dirs = [ 'include', 'headers' ]


let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlPCurWD'
set wildignore+=*/release/*,*/build/*,*.swp,*.zip,*.so


let g:DoxygenToolkit_authorName="Tran Tuan Hiep" 
let g:DoxygenToolkit_licenseTag="GNU"
let g:tex_flavor='latex'
let b:tex_flavor = 'pdflatex'
compiler tex
set makeprg=pdflatex\ \-file\-line\-error\ \-interaction=nonstopmode
set errorformat=%f:%l:\ %m

" nastaveni ctrlp
let g:ctrlp_working_path_mode = 'ra'


au FileType c,cpp,objc,objcpp call rainbow#load()



let mapleader="\<Space>"

autocmd FileType cpp nmap<F3> :Dox<CR>

" tab navigation

" zkratky pro tex
autocmd FileType tex nmap <F6> :Latexmk<CR>
autocmd FileType tex nnoremap <C-s> :w<CR>a
autocmd FileType tex inoremap  <C-s> <esc>:w<CR>a

" zkratky ovladani
nnoremap <leader>jd :YcmCompleter GoTo<CR>
nnoremap <leader>lc :VimuxRunLastCommand<CR>
nnoremap <leader>rc :VimuxRunCommand
inoremap jj <ESC>
nnoremap <leader>cpf :CtrlPCurFile<CR>
nnoremap <leader>tn :tabn<CR>
nnoremap <leader>tp :tabp<CR>
nnoremap <C-t> :tabnew<CR>
nnoremap <leader>N :NERDTree<CR>
nnoremap <leader>tgb :TagbarToggle<CR>
nnoremap <leader>w :w<CR>
nnoremap <leader>hl :nohl<CR>
nnoremap <F9> :GundoToggle<CR> 
" ovladani terima 
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)


" noremap {      {}<Left>
" inoremap {<CR>  {<CR>}<Esc>O
" inoremap {{     {
" inoremap {}     {}
" 
" inoremap (      ()<Left>
" inoremap (<CR>  (<CR>)<Esc>O
" inoremap ((     (
" inoremap ()     ()
" 
" inoremap [      []<Left>
" inoremap [<CR>  [<CR>]<Esc>O
" inoremap [[     [
" inoremap []     []
colorscheme molokai



